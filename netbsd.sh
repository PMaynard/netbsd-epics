#!/bin/sh
set -e

function LOG () {
	echo "[>] $1"
}

NETBSD_VERSION=8.1
SRC_FILES="SHA512 gnusrc.tgz sharesrc.tgz src.tgz syssrc.tgz xsrc.tgz"

# TODO: Place all NetBSD sources into their own directory, maybe ./vendor ?

LOG "NetBSD $NETBSD_VERSION"

# 
#  	Download Sources
# 
LOG "Downloading files"
for F in $SRC_FILES
do
	# -q: Quiet. -c: continues partial files. -N: Only downloads if changed.
	wget -q --show-progress -c -N ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-$NETBSD_VERSION/source/sets/$F
done

LOG "Checking Hashes"
sha512sum -c SHA512

LOG "Extracting Archives"
for F in *.tgz
do 
	# TODO: Only extract if it needs too.
	tar xfz $F
done

#
#	Build Cross Compiler
#
LOG "Building Cross Compiler"
cd usr/src
./build.sh -U -u -m amd64 tools

#
#	Build NetBSD
#
LOG "Building NetBSD $NETBSD_VERSION"
./build.sh -U -u -m amd64 release

LOG "Creating ISO image"
./build.sh -U -u -m amd64 iso-image
