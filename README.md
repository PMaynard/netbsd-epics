# NetBSD EPICS

A collection of scripts that downloads, compiles, then creates a NetBSD VM for use with [EPICS](https://epics-controls.org).

- `netbsd.sh`
	- Download NetBSD sources, builds the cross compiler tools and builds the iso.
- `instantiate-node.sh`
	- Creates a pre-configured NetBSD image for running EPICS.
- `epics.sh`
	- Downloads EPICS sources, and builds EPICS.

# Run

These scripts have been developed on Fedora Server 31.  
It is recommended to build NetBSd within tmux, it will take a long time.

	tmux new-session -s netbsd-build 'sh netbsd.sh;sh -i'

# Dependencies

These scripts need:

- tmux
- wget
- anita

NetBSD needs: 

- make 
- automake
- gcc
- gcc-c++
- zlib-devel
- flex
- kernel-devel

Anita needs: 

- python3-pexpect

## Install all dependencies

- Fedora/RedHat/CentOS

	sudo dnf install make automake gcc gcc-c++ zlib-devel flex python3-pexpect

- Debian/Ubuntu/Mint

	sudo apt install build-essentials zlib1g-dev flex python3-pexpect

# Thanks To

Everyone single person working on these projects!  
Also these specific pages:

- <https://www.cambus.net/cross-building-netbsd-on-linux/>
- <https://prjemian.github.io/epicspi/>
- <http://www.gson.org/netbsd/anita/> 

Grep for 'TODO' for things which need improving.

# License

Copyright 2019 Peter Maynard

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
