#!/bin/sh
set -e

LOG () {
	echo "[>] $1"
}

EPICS_VERSION=7.0.3.1

#
# Create EPICS expected directories.
#
LOG "Creating the directories"
CURWD=$(pwd)
cd $HOME
mkdir -p epics
mkdir -p /usr/local/
ln -vnfs $HOME/epics /usr/local/epics
cd epics

#
# Download, verify, and extract EPICS.
#
LOG "Downloading files"
wget -c -N http://www.aps.anl.gov/epics/download/base/base-$EPICS_VERSION.tar.gz
wget -c -N https://epics.anl.gov/download/base/base-$EPICS_VERSION.tar.gz.asc

LOG "Verifying Signatures"
gpg --recv 6C741EFAAE4AF616
gpg --verify base-$EPICS_VERSION.tar.gz.asc base-$EPICS_VERSION.tar.gz

LOG "Extract Tar"
# TODO: Keep compressed archive to prevent wget from re-downloading
gzip -d base-$EPICS_VERSION.tar.gz
tar xf base-$EPICS_VERSION.tar
ln -vfns ./base-$EPICS_VERSION ./base

#
# Set host arch
#

export EPICS_HOST_ARCH=$(perl /usr/local/epics/base/lib/perl/EpicsHostArch.pl)
LOG "Building EPICS $EPICS_VERSION for $EPICS_HOST_ARCH"

#
# Build
#

cd base
make

#
# Done
#
LOG "EPICS has been built"
LOG "Check it is working by running 'epics/base/bin/$EPICS_HOST_ARCH/softIoc' "
LOG "Then typing 'iocInit' "
LOG "You might want to add the following to your '.bash_aliases': "
echo ""
cd $CURWD
cat epics_bash_aliases
