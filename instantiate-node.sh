#!/bin/sh
set -e

function LOG () {
	echo "[>] $1"
}

function GIT_CLONE () {
	if ! git clone "$1" "$2" && [ -d "$2" ] ; then
    	LOG "Folder $2 exists, pulling changes"
    	git -C $2 pull 
	fi
}

VENDOR_DIR=vendor
ANITA_WORKDIR=anita-workdir
ANITA_VMM=qemu
ANITA_LOG=anita.log


#
# Get anita
#
LOG "Getting anita source"
GIT_CLONE https://github.com/gson1703/anita.git $VENDOR_DIR/anita

#
# Install anitia
#
LOG "Installing anita"
# TODO: (pip) Don't run setup if src version and installed version are the same.
pip install --user $VENDOR_DIR/anita
# TODO: Check the 'anita' command is found and works

#
# Setup node
#
LOG "Creating a VM image"
anita install file://$(pwd)/usr/src/obj/releasedir/amd64/ --workdir "$ANITA_WORKDIR --vmm $ANITA_VMM --structured-log-file $ANITA_LOG"

LOG "VM image created."
LOG "Run the below to get a interactive shell:"
LOG " 'anita interact file://$(pwd)/usr/src/obj/releasedir/amd64/ --workdir $ANITA_WORKDIR' "
